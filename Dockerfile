FROM debian:jessie
MAINTAINER Michal Belica <devel@beli.sk>

EXPOSE 10000

# add unstable repo and set pins
RUN echo "deb http://httpredir.debian.org/debian unstable main" >> /etc/apt/sources.list \
	&& echo "Package: *\nPin: release a=unstable\nPin-Priority: 800" >> /etc/apt/preferences

RUN apt-get update \
	&& apt-get install -y --no-install-recommends armory xpra \
	&& apt-get clean && rm -rf /var/lib/apt/lists/*
RUN sed -ie 's/^start-child/#start-child/' /etc/xpra/xpra.conf
RUN useradd -d /home/user -m user \
	&& mkdir /armory && chown user: /armory \
	&& mkdir /bitcoin && chown user: /bitcoin
USER user
ENV HOME /home/user
WORKDIR /home/user

VOLUME /armory
VOLUME /bitcoin/data

CMD ["/usr/bin/xpra", "start", ":100", "--start-child=/usr/bin/armory --datadir=/armory --satoshi-datadir=/bitcoin/data", "--bind-tcp=0.0.0.0:10000", "--no-daemon", "--no-notifications", "--no-mdns", "--no-pulseaudio", "--exit-with-children"]
